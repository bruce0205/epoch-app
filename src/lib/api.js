import axios from 'axios'
import store from '@/store'
import md5 from 'js-md5'
const SERVER_URL = process.env.VUE_APP_API

const api = {
  hello () {
    const url = `${SERVER_URL}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    axios.get(url, { params, headers })
      .then((response) => {
        console.log(response)
      }).catch((err) => {
        console.error(err)
      })
  },
  async getMachine () {
    try {
      const url = `${SERVER_URL}/Machine/GetMachine`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async getMachineStatus (machineNo) {
    try {
      console.log('api.getMachineStatus()')
      const url = `${SERVER_URL}/Machine/GetMachineStatus/${store.state.account}/${machineNo}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async getPartNo (machineNo) {
    try {
      console.log('api.getPartNo()')
      const url = `${SERVER_URL}/MFG/GetPartNo/${store.state.account}/${machineNo}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async getTryMoldReason () {
    try {
      const url = `${SERVER_URL}/Machine/GetTryMoldReason`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updateTryMoldReason (tryMoldNo, reason) {
    try {
      const url = `${SERVER_URL}/Machine/UpdateMoldReason/${tryMoldNo}/${reason}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async getMfgErrorType (partNo, mold) {
    try {
      console.log('api.getMfgErrorType()')
      const url = `${SERVER_URL}/MFG/GetMfgErrorType/${store.state.account}/${partNo}/${mold}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updateMachineStatus (machineNo, machineStage, partNo, mold) {
    try {
      console.log('api.updateMachineStatus()')
      const url = `${SERVER_URL}/Machine/UpdateMachineStatus/${store.state.account}/${machineNo}/${machineStage}/${partNo}/${mold}`
      // let params = {}
      // let headers = { 'Access-Control-Allow-Origin': '*' }
      // const response = await axios.get(url, { params, headers })
      // return response
      console.log("aaaaaaa")
      const response = await fetch(url, { method: 'GET', credentials: "same-origin"})
      const result = await response.json()
      console.log(result)
      return result
    } catch (err) {
      throw new Error(err)
    }
  },
  async createMachineRemark (machineNo, partNo, mold, remark) {
    try {
      console.log('api.createMachineRemark()')
      const url = `${SERVER_URL}/Machine/CreateRemark/${store.state.account}/${machineNo}/${partNo}/${mold}/${remark}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updatePartNo (machineNo, partNo, mold) {
    try {
      console.log('api.updatePartNo()')
      const url = `${SERVER_URL}/MFG/UpdatePartNo/${store.state.account}/${machineNo}/${partNo}/${mold}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response.data
    } catch (err) {
      throw new Error(err)
    }
  },
  async doLogin (account, password, needEncrypt = true) {
    try {
      console.log('api.doLogin()')
      if (needEncrypt) {
        password = md5(password).toUpperCase()
      }
      const url = `${SERVER_URL}/Login/DoLogin/${account}/${password}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updateMfgErrorCount (machineNo, shift, hole, errorTypeId, errorCount) {
    try {
      console.log('api.updateMfgErrorCount()')
      const url = `${SERVER_URL}/MFG/UpdateMfgErrorCount/${store.state.account}/${machineNo}/${shift}/${hole}/${errorTypeId}/${errorCount}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updateHole (machineNo, hole, shift, isOnline) {
    try {
      console.log('api.updateHole()')
      const url = `${SERVER_URL}/MFG/UpdateHole/${store.state.account}/${machineNo}/${hole}/${shift}/${isOnline}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async getHole (machineNo) {
    try {
      console.log('api.getHole()')
      const url = `${SERVER_URL}/MFG/GetHole/${store.state.account}/${machineNo}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response
    } catch (err) {
      throw new Error(err)
    }
  },
  async updateMachineCount (machineNo, shift, machineCount) {
    try {
      console.log('api.updateMachineCount()')
      const url = `${SERVER_URL}/MFG/UpdateMachineCount/${store.state.account}/${machineNo}/${shift}/${machineCount}`
      let params = {}
      let headers = { 'Access-Control-Allow-Origin': '*' }
      const response = await axios.get(url, { params, headers })
      return response.data
    } catch (err) {
      throw new Error(err)
    }
  }
}

export default api
