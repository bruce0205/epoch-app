import { register } from 'register-service-worker'
import store from './store'

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    registered (registration) {
      console.log('Service worker has been registered.')
      console.log(registration)
    },
    cached (registration) {
      console.log('Content has been cached for offline use.')
    },
    updatefound (registration) {
      console.log('New content is downloading.')
    },
    updated (registration) {
      console.log('New content is available; please refresh.')
      console.log(registration)
      console.log(store.state.updatable)
      try {
        store.commit('setUpdatable', true)
        let newWorker = registration.waiting
        console.log(newWorker)
        newWorker.addEventListener('message', function (event) {
          console.log('aaa')
          if (event.data.action === 'skipWaiting') {
            console.log('bbb')
            console.log(self)
            self.skipWaiting()
          }
        })
        newWorker.postMessage({ action: 'skipWaiting' })
        console.log('6666')
      } catch (err) {
        console.error(err)
      }
    },
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
