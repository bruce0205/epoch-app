import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/card',
      name: 'card',
      component: () => import('./views/Card.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/scan',
      name: 'scan',
      component: () => import('./views/Scan.vue')
    },
    {
      path: '/carousels',
      name: 'carousels',
      component: () => import('./views/Carousels.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/index.html',
      name: 'index',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/',
      name: 'root',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/list',
      name: 'list',
      component: () => import('./views/MachineList.vue')
    },
    {
      path: '/manufacture',
      name: 'manufacture',
      component: () => import('./views/Manufacture.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./views/Test.vue')
    }
  ]
})
