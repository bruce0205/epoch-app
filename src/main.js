import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueQrcodeReader from 'vue-qrcode-reader'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(VueQrcodeReader)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
