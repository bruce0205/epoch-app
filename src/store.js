import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogin: false,
    username: '',
    account: '',
    group: '',
    shift: '',
    updatable: false,
    machineNo: '',
    plusCount: 1,
    mold: '',
    cto: 0,
    hole: '',
    hole1: '',
    hole2: '',
    partNo: ''
  },
  mutations: {
    setIsLogin (state, isLogin) {
      state.isLogin = isLogin
    },
    setUpdatable (state, updatable) {
      state.updatable = updatable
    },
    setUsername (state, username) {
      state.username = username
    },
    setAccount (state, account) {
      state.account = account
    },
    setGroup (state, group) {
      state.group = group
    },
    setMold (state, mold) {
      state.mold = mold
    },
    setCto (state, cto) {
      state.cto = cto
    },
    setHole (state, hole) {
      state.hole = hole
    },
    setHole1 (state, hole1) {
      state.hole1 = hole1
    },
    setHole2 (state, hole2) {
      state.hole2 = hole2
    },
    setPartNo (state, partNo) {
      state.partNo = partNo
    },
    setMachineNo (state, machineNo) {
      state.machineNo = machineNo
    },
    setPlusCount (state, plusCount) {
      state.plusCount = plusCount
    },
    setShift (state, shift) {
      state.shift = shift
    }
  },
  actions: {
  }
})
