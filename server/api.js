const express = require('express')
const cors = require('cors')
const axios = require('axios')
const swStats = require('swagger-stats')

const app = express()
const port = 3456
const SERVER_URL = 'https://638d-61-216-20-235.ngrok-free.app'
const apiSpec = require('./swagger.json')

app.use(cors())
app.use(swStats.getMiddleware({ swaggerSpec: apiSpec }))

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/test', (req, res) => {
  res.send({ message: 'ok' })
})

app.get('/Machine/GetMachine', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Machine/GetMachine`
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Machine/GetTryMoldReason', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Machine/GetTryMoldReason`
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Machine/GetMachineStatus/:account/:machineNo', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Machine/GetMachineStatus/${req.params.account}/${req.params.machineNo}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Machine/UpdateMoldReason/:tryMoldNo/:reason', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Machine/UpdateMoldReason/${req.params.tryMoldNo}/${encodeURIComponent(req.params.reason)}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/GetPartNo/:account/:machineNo', async (req, res) => {
  try {
    const url = `${SERVER_URL}/MFG/GetPartNo/${req.params.account}/${req.params.machineNo}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/GetMfgErrorType/:account/:partNo/:mold', async (req, res) => {
  try {
    const url = `${SERVER_URL}/MFG/GetMfgErrorType/${req.params.account}/${req.params.partNo}/${req.params.mold}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Machine/UpdateMachineStatus/:account/:machineNo/:machineStage/:partNo/:mold', async (req, res) => {
  try {
    let url = `${SERVER_URL}/Machine/UpdateMachineStatus/${req.params.account}/${req.params.machineNo}/${req.params.machineStage}/${req.params.partNo}/${req.params.mold}`
    url = encodeURI(url)
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/UpdatePartNo/:account/:machineNo/:partNo/:mold', async (req, res) => {
  try {
    const url = `${SERVER_URL}/MFG/UpdatePartNo/${req.params.account}/${req.params.machineNo}/${req.params.partNo}/${req.params.mold}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Machine/CreateRemark/:account/:machineNo/:partNo/:mold/:remark', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Machine/CreateRemark/${req.params.account}/${req.params.machineNo}/${req.params.partNo}/${req.params.mold}/${req.params.remark}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/Login/DoLogin/:account/:password', async (req, res) => {
  try {
    const url = `${SERVER_URL}/Login/DoLogin/${req.params.account}/${req.params.password}`
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/UpdateMfgErrorCount/:account/:machineNo/:shift/:hole/:errorTypeId/:errorCount', async (req, res) => {
  try {
    let url = `${SERVER_URL}/MFG/UpdateMfgErrorCount/${req.params.account}/${req.params.machineNo}/${req.params.shift}/${req.params.hole}/${req.params.errorTypeId}/${req.params.errorCount}`
    url = encodeURI(url)
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/UpdateHole/:account/:machineNo/:hole/:shift/:isOnline', async (req, res) => {
  try {
    let url = `${SERVER_URL}/MFG/UpdateHole/${req.params.account}/${req.params.machineNo}/${req.params.hole}/${req.params.shift}/${req.params.isOnline}`
    url = encodeURI(url)
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/GetHole/:account/:machineNo', async (req, res) => {
  try {
    let url = `${SERVER_URL}/MFG/GetHole/${req.params.account}/${req.params.machineNo}`
    url = encodeURI(url)
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.get('/MFG/UpdateMachineCount/:account/:machineNo/:shift/:machineCount', async (req, res) => {
  try {
    let url = `${SERVER_URL}/MFG/UpdateMachineCount/${req.params.account}/${req.params.machineNo}/${req.params.shift}/${req.params.machineCount}`
    url = encodeURI(url)
    console.log(url)
    let params = {}
    let headers = { 'Access-Control-Allow-Origin': '*' }
    const response = await axios.get(url, { params, headers })
    res.send(response.data)
  } catch (err) {
    throw new Error(err)
  }
})

app.listen(port, () => console.log(`Server is listening at http://localhost:${port}`))
